defmodule UtilsFor.MixProject do
  use Mix.Project

  @package_name "Utils For"

  @version "0.0.0"

  @source_url "https://gitlab.com/exadra37-elixir/packages/utils-for"

  @homepage_url "https://gitlab.com/exadra37-elixir/packages/utils-for/blob/master/README.md"

  @description """
  UtilsFor is a collection of utils for removing code duplication and the need to keep code snippets around.

  Some utils will wrap and simplify usage of the core language and/or external dependencies.

  Other utils will add new functionality that must be agnostic of any project.

  > **NOTE**: This package will not include the dependencies for you.
  """

  def project() do
    [
      app: :utils_for,
      version: @version,
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      build_embedded: Mix.env == :prod,
      deps: deps(),

      # Hex
      package: package(),
      description: @description,
      source_url: @source_url,
      homepage_url: @homepage_url,

      # Docs
      name: @package_name,
      docs: [
        source_ref: "v#{@version}",
        source_url: @source_url,
        main: "readme",
        extras: ["README.md"]
      ],
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    []
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.21", only: :dev, runtime: false}
    ]
  end

  defp package() do
    [
      name: @package_name,

      maintainers: ["Exadra37"],

      # These are the default files included in the package
      files: ~w(lib priv .formatter.exs mix.exs README* readme* LICENSE*
                license* CHANGELOG* changelog* src),
      licenses: ["MIT"],
      links: %{"Gitlab" => @source_url}
    ]
  end
end
