defmodule UtilsFor do
  @moduledoc """
  Documentation for UtilsFor.
  """

  @doc """
  Hello world.

  ## Examples

      iex> UtilsFor.hello()
      :world

  """
  def hello do
    :world
  end
end
